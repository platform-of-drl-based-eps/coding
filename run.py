import os
import gym
import numpy as np
import matplotlib.pyplot as plt
from stable_baselines3 import A2C,DQN,PPO
from stable_baselines3.common import results_plotter
from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.results_plotter import load_results, ts2xy, plot_results
from stable_baselines3.common.noise import NormalActionNoise
from stable_baselines3.common.callbacks import BaseCallback
from environment_EPS import PFEnv
import json
from stable_baselines3.common.evaluation import evaluate_policy
from typing import Callable

spaces_text = open("DC_DC.json",encoding="gb18030",errors="ignore")
dicd=json.load(spaces_text)
learning_times=int(dicd["learning_times"])
test_times=int(dicd["test_times"])
total_time_step=int(dicd["total_time_step"])

monitor_dir = r'./monitor_log/'
os.makedirs(monitor_dir,exist_ok=True)
env = PFEnv()
env = Monitor(env, monitor_dir)
tensorboard_log = r'./tensorboard_log/'

# Setting hyperparameters
model = PPO("MlpPolicy", env, verbose=1,batch_size=16,n_steps=128,policy_kwargs=dict(net_arch=[dict(vf=[256,256,256], pi=[256,256,256])]),
           learning_rate= 0.00029125,tensorboard_log=tensorboard_log)

class SaveOnBestTrainingRewardCallback(BaseCallback):
    """
    Callback for saving a model (the check is done every ``check_freq`` steps)
    """
    def __init__(self, check_freq, save_model_dir, verbose=1):
        super(SaveOnBestTrainingRewardCallback, self).__init__(verbose)
        self.check_freq = check_freq
        self.save_path = os.path.join(save_model_dir, 'best_model/')
        self.best_mean_reward = -np.inf

    # def _init_callback(self) -> None:
    def _init_callback(self):
        # Create folder if needed
        if self.save_path is not None:
            os.makedirs(self.save_path, exist_ok=True)

    # def _on_step(self) -> bool:
    def _on_step(self):
        if self.n_calls % self.check_freq == 0:
            print('self.n_calls: ',self.n_calls)
            model_path1 = os.path.join(self.save_path, 'model_{}'.format(self.n_calls))
            self.model.save(model_path1)

        return True

# Where to save the model
save_model_dir = r'C:\Users\user\Desktop\DC_DC\history'
callback1 = SaveOnBestTrainingRewardCallback(10000, save_model_dir)

# training without model
model.learn(total_timesteps=learning_times)
model.save("model")

# training with model
# model = PPO.load("model",env)
# model.learn(total_timesteps=learning_times,callback=callback1)
# model.save("mario_model")

# application
# model = PPO.load("model",env)
# mean_reward, _ = evaluate_policy(model, env, n_eval_episodes=20,deterministic= True)