import gym
from gym import spaces
import numpy as np
import time
import math
import os
import copy

"""
Path to the PF installation file
"""
pf_install_path = r'D:\PF2022'

os.environ["PATH"]=pf_install_path+os.environ["PATH"]
import sys
sys.path.append(pf_install_path + r'\Python\3.7')
import powerfactory as pf
import gym
from gym import spaces
import pandas as pd
import random
import json
import time
import matplotlib.pyplot as plt
from numpy import trapz
import powerfactory
from xlutils.copy import copy
from sklearn import preprocessing
spaces_text = open("parameters_EPS.json")
dicd=json.load(spaces_text)

#仿真设置相关
action_step_time = float(dicd["action_step_time"]) # interaction interval
sim_step_size = float(dicd["sim_step_size"])  # Simulation step(ms)
sim_output_step = int(dicd["sim_output_step"])  # Simulation step multiplier
sim_method = dicd["sim_method"]   # RMS simulation
sim_start_time = float(dicd["sim_start_time"]) # Initialization is performed from how many seconds
net_rep = dicd["net_rep"]  # unbalanced,3 -phase="rst" balanced=“sym”
dic_mv=dicd["dic_mv"]
total_time_step=int(dicd["total_time_step"])    # How many interactions are performed on a single training episode
backward_observation_step = int(dicd["backward_observation_step"])  # How many observations will be looked at in each interaction
sim_frist_step= float(dicd["sim_frist_step"])   # Initialization runs up to how many seconds
operating_point=dicd["operating_point"]     # Name of the example to be run (name of the pdf in the folder)
observation_col=dicd["observation_col"]     # Name of the observed data
delete_model_time=int(dicd["delete_model_time"])    # How many interactions to reset the algorithm cache or change the algorithm training
DC_action_time=float(dicd["DC_action_time"])    # EPS Execution Time
prob_location = dicd["prob_location"]   # Fault location
prob_type = dicd["prob_type"]   # Fault type
prob_time = dicd["prob_time"]   # Fault time

import copy
frist_project="IEEE39"   # Example name in dig (not in folder)
DC_low=int(dicd["DC_low"])  # DC Power Support Lower Limit
DC_high=int(dicd["DC_high"])    # DC Power Support Upper Limit


class PFEnv(gym.Env):
    def render(self, mode="human"):
        pass

    def __init__(self):
        super(PFEnv, self).__init__()
        self.app = powerfactory.GetApplication()
        self.app.ActivateProject(frist_project)
        self.delete_model()
        self.DC_support_t = "False"
        self.DC_support_t = dicd["DC_support_t"]
        self.generator_out_t = "False"
        self.generator_out_t = dicd["generator_out_t"]

        DC_action_spaces_num = int((DC_high-DC_low)/10)

        # action space
        self.action_space = spaces.Discrete(int(DC_action_spaces_num)+1)

        # state space
        self.observation_space = spaces.Box(low=0, high=0, shape=(int(backward_observation_step),), dtype=np.float32)

        self.history_step_time = 1
        self.total_time_step=total_time_step
        self.prob = 0
        self.one_prob = 0
        self.step_ = 1
        self.total_step_ = 1
        self.penalty_label = 0
        self.prob_location_r=0
        self.delete_model_times=1
        self.break_report = 0
        self.state_num = 1
        self.action_num = 1

    def delete_model(self):
        """
        In order to simulate the system load distribution changes, can be stored in the path of the pdf file
        to store a number of different system load distribution of the pdf file, the function is used in the
        training process to randomly select the system operating state
        """
        random_operation = random.randint(0, len(operating_point)-1)
        act_project = self.app.GetActiveProject()
        act_project.Deactivate()
        act_project.Delete()
        self.app.ClearRecycleBin()
        cur_user = self.app.GetCurrentUser()
        importObj = cur_user.CreateObject('CompfdImport', 'Import')

        """
        Path to pdf instance storage
        """
        importObj.g_file = r"C:\Users\user\Desktop\庄\DC_DC\operating_point\\"+operating_point[random_operation]+".pfd"
        importObj.g_target = cur_user
        importObj.Execute()
        importObj = cur_user.GetContents('Import.CompfdImport')[0]
        importObj.Delete()
        self.app.ActivateProject(frist_project)

        self.res_set_monitored_variables = self.app.GetFromStudyCase('All calculations.ElmRes')
        self.res_get_results = self.app.GetFromStudyCase('All calculations.Elmres')
        self.init_pf_ini = self.app.GetFromStudyCase("ComInc")
        self.events = self.app.GetFromStudyCase('Simulation Events/Fault.IntEvt')
        self.Sim_event_folder = self.app.GetFromStudyCase('IntEvt')
        self.sim = self.app.GetFromStudyCase("ComSim")
        self.export = self.app.GetFromStudyCase('Result Export.ComRes')
        self.res = self.app.GetFromStudyCase('All calculations.Elmres')
        self.set_monitored_variables(dic_mv)

        self.generators = self.app.GetCalcRelevantObjects('*.ElmSym')
        self.generators_dic = {}
        for q in range(len(self.generators)):
            self.generators_dic[self.generators[q].loc_name] = q

        self.generator_h_dic = {}
        for i in range(len(self.generators)):
            self.generator_h_dic[self.generators[i].loc_name] = self.generators[i].typ_id.h
            self.generator_h_df = pd.DataFrame(self.generator_h_dic, index=[i])

        self.observation_col = observation_col
        self.observation_col.sort()
        self.df_episode = pd.DataFrame(columns=self.observation_col)


    def set_monitored_variables(self, dic_mv):
        """
    This function will set the monitored variables in the activated test system. It will first clear all the monitored variables in the all calculations folder.
        :param dic_mv:a dictionary of monitored variables,where keys are the names of monitored object, and values are the variables of the object
        """
        monitor_folder = self.res_set_monitored_variables.GetContents()
        for Intmon in monitor_folder:
            Intmon.Delete()
        for ele in dic_mv.keys():
            elements = self.app.GetCalcRelevantObjects(ele)
            for element in elements:
                self.res_set_monitored_variables.AddVars(element, *dic_mv[ele])

    def delete_all_events(self):
        """
    This function will delete all the simulation events or all the contents in the Simulation Event folder
        """
        event_folder = self.events.GetContents()
        for event in event_folder:
            event.Delete()

    def creat_events(self, evt_type, name):
        """
    This function will creat simulation events in digsilent. It returns diffenent eventset object.
        :param evt_type: event type in digsilent,such as 'EvtShc','EvtGen','EvtLod'......
        :param name: string, name of the event
        """
        self.Sim_event_folder.CreateObject(evt_type, name)
        event = self.Sim_event_folder.GetContents(name + '.' + evt_type)[0]
        return event


    def pf_ini(self, sim_step_size, ratio_of_out_sim_step, sim_method, net_rep, sim_start_time):
        """
    This function will complete the initialization and start the simulation in Digsilent.The event object floatEvt, which contains different envents created func creat_eventsby and the name of floatEvt must be Simulation Events/Fault.Results objective, which contains the variables list and simulation results, and the name must be All calculations by default/
        :param sim_method: Simulation method, 'rms' for electromechanical transients
        :param net_rep: network representation for initialization, unbalanced,3 -phase="rst" balanced=“sym”
        :param record_start_time: the starting time for recording in the simulation results file
        :param sampling_step: the sampling time for the simulation results file
        :param sim_time: the time that the simulation stops or lasts
        """
        # simulation initialization
        # it will execute the power flow automatically
        self.init_pf_ini.iopt_sim = sim_method
        self.init_pf_ini.dtgrd = sim_step_size
        self.init_pf_ini.iopt_net = net_rep
        self.init_pf_ini.tstart = sim_start_time
        self.init_pf_ini.ciopt_sample = 1
        self.init_pf_ini.mult_out = ratio_of_out_sim_step
        if self.init_pf_ini.p_resvar.loc_name == 'All calculations' and self.init_pf_ini.p_event.loc_name == 'Simulation Events/Fault':
            self.init_pf_ini.Execute()
        else:
            # print('The program is exited.\n Please set "All calculations" as the "Elmres" and "Simulation Events/Fault" as the "IntEvt"!')
            sys.exit(0)
    #pf_ini(sim_step_size=1, ratio_of_out_sim_step=2, sim_method='rms', net_rep='sym', sim_start_time=-50)

    def pf_sim(self, sim_time):
        self.sim.tstop = sim_time
        self.break_report=self.sim.Execute()

    def get_results(self,backward_observation_step):
        # Reading of observation data
        obs_=[]
        self.export.pResult = self.res
        self.export.iopt_csel = 0
        self.export.iopt_exp = 6
        self.export.iopt_tsel = 0
        self.export.iopt_locn = 1
        self.export.ciopt_head = 1
        temppath = os.getcwd() + r'\temp_results.csv'
        self.export.f_name = temppath
        self.export.Execute()
        # read two rows to get the colums_name
        title = pd.read_csv(temppath, header=None, nrows=2)
        names = title.T[0] + '///' + title.T[1]
        results = pd.read_csv(temppath, header=None, skiprows=2)
        results.columns = names
        results = results.sort_index(axis=1)
        results.drop(results.columns[0], axis=1, inplace=True)
        if list(results.columns) == list(self.df_episode.columns):
            self.df_episode = self.df_episode.append(results, ignore_index=True)
        else:
            diff = list(set(list(results.columns)) ^ set(self.df_episode.columns))
            for i in range(len(diff)):
                results.loc[:, diff[i]] = 0
                results = results.sort_index(axis=1)
                self.df_episode = self.df_episode.append(results, ignore_index=True)
        states_return = self.df_episode.iloc[(self.df_episode.shape[0] - backward_observation_step):].copy()
        self.bus_f = states_return[["Bus 1///m:fehz in Hz","Bus 2///m:fehz in Hz","Bus 3///m:fehz in Hz","Bus 4///m:fehz in Hz","Bus 5///m:fehz in Hz",
		"Bus 6///m:fehz in Hz","Bus 7///m:fehz in Hz","Bus 8///m:fehz in Hz","Bus 9///m:fehz in Hz","Bus 10///m:fehz in Hz",
		"Bus 11///m:fehz in Hz","Bus 12///m:fehz in Hz","Bus 13///m:fehz in Hz","Bus 14///m:fehz in Hz","Bus 15///m:fehz in Hz",
		"Bus 16///m:fehz in Hz","Bus 17///m:fehz in Hz","Bus 18///m:fehz in Hz","Bus 19///m:fehz in Hz","Bus 20///m:fehz in Hz",
		"Bus 21///m:fehz in Hz","Bus 22///m:fehz in Hz","Bus 23///m:fehz in Hz","Bus 24///m:fehz in Hz","Bus 25///m:fehz in Hz",
		"Bus 26///m:fehz in Hz","Bus 27///m:fehz in Hz","Bus 28///m:fehz in Hz","Bus 29///m:fehz in Hz","Bus 30///m:fehz in Hz",
		"Bus 31///m:fehz in Hz","Bus 32///m:fehz in Hz","Bus 33///m:fehz in Hz","Bus 34///m:fehz in Hz","Bus 35///m:fehz in Hz",
		"Bus 36///m:fehz in Hz","Bus 37///m:fehz in Hz","Bus 38///m:fehz in Hz","Bus 39///m:fehz in Hz"]]
        self.line_P=states_return[["Line 01 - 02///m:P:bus2 in MW","Line 01 - 39///m:P:bus2 in MW","Line 02 - 03///m:P:bus2 in MW","Line 02 - 25///m:P:bus2 in MW","Line 03 - 04///m:P:bus2 in MW",
		"Line 03 - 18///m:P:bus2 in MW","Line 04 - 05///m:P:bus2 in MW","Line 04 - 14///m:P:bus2 in MW","Line 05 - 06///m:P:bus2 in MW","Line 05 - 08///m:P:bus2 in MW",
		"Line 06 - 07///m:P:bus2 in MW","Line 06 - 11///m:P:bus2 in MW","Line 07 - 08///m:P:bus2 in MW","Line 08 - 09///m:P:bus2 in MW","Line 09 - 39///m:P:bus2 in MW",
		"Line 10 - 11///m:P:bus2 in MW","Line 10 - 13///m:P:bus2 in MW","Line 13 - 14///m:P:bus2 in MW","Line 14 - 15///m:P:bus2 in MW","Line 15 - 16///m:P:bus2 in MW",
		"Line 16 - 17///m:P:bus2 in MW","Line 16 - 19///m:P:bus2 in MW","Line 16 - 21///m:P:bus2 in MW","Line 16 - 24///m:P:bus2 in MW","Line 17 - 18///m:P:bus2 in MW",
		"Line 17 - 27///m:P:bus2 in MW","Line 21 - 22///m:P:bus2 in MW","Line 22 - 23///m:P:bus2 in MW","Line 23 - 24///m:P:bus2 in MW","Line 25 - 26///m:P:bus2 in MW",
		"Line 26 - 27///m:P:bus2 in MW","Line 26 - 28///m:P:bus2 in MW","Line 26 - 29///m:P:bus2 in MW","Line 28 - 29///m:P:bus2 in MW"]]
        self.U = states_return[["Bus 30///m:U in Hz"]]
        self.x = states_return[["Rect Controller_HVDC2///s:x"]]
        self.i = states_return[["LineDC_I///m:I:bus1 in kA"]]
        self.sa = states_return[["Rect Controller_HVDC2///s:alpha_R"]]
        self.a = states_return[["RectY///Attribute:gammamindyn"]]

        # state gradient
        state = copy.deepcopy(self.bus_f.values)
        for a in range(backward_observation_step):
            state[a] = state[a] - 48
        # normalize
        for i in range(backward_observation_step):
            to_one = state[i]
            x = np.array([[0],
                          [to_one[0]],
                          [4]])
            max_abs_scaler = preprocessing.MaxAbsScaler()
            y = max_abs_scaler.fit_transform(x)
            y_copy = (((y[1][0].copy()) * 10) - 4.8) * 100
            obs_.append((round(y_copy, 3)))
        round(obs_[1], 3)
        return obs_

    def pf_forward(self):
        # Control emulation stepping
        self.pf_ini(sim_step_size, sim_output_step, sim_method, net_rep, sim_start_time)
        self.sim.LoadSnapshot()
        self.pf_sim(self.history_step_time*action_step_time+sim_frist_step)
        self.sim.SaveSnapshot()
        self.history_step_time+=1
        states_return = self.get_results(backward_observation_step)
        return states_return


    def reset(self):
        """
        Initialization function required before the start of each training set to add random faults
        and determine whether to replace the power system model
        """
        if self.delete_model_times==1:
            self.delete_model()
            self.delete_model_times=0

        self.df_episode = pd.DataFrame(columns=self.observation_col)
        self.delete_all_events()
        self.pf_ini(sim_step_size, sim_output_step, sim_method, net_rep, sim_start_time)
        self.history_step_time=1
        self.step_=1
        self.penalty_line = 0
        self.generator_out_t=False

        random_pro = random.randint(0,1)
        if random_pro == 0:
            self.generator_out(-1)
        if random_pro == 1:
            self.new_out()

        self.pf_ini(sim_step_size, sim_output_step, sim_method, net_rep, sim_start_time)
        self.pf_sim(sim_frist_step)
        observation = self.get_results(backward_observation_step)
        return observation

    def new_out(self):
        # Control of new energy tripping
        new_location = dicd["new_location"]
        random_new = random.randint(0, len(new_location) - 1)
        new_out_object = self.app.GetCalcRelevantObjects(new_location[random_new])[0]
        dis_event = self.creat_events("EvtOutage", 'Generator_Trip')
        dis_event.time = float(prob_time[0])
        dis_event.p_target = new_out_object

    def generator_out(self,location):
        # Control of generator tripping
        random_location = random.randint(0, len(prob_location) - 1)
        random_time = random.randint(0, len(prob_time) - 1)
        self.prob_location_r = prob_location[random_location] # fault location
        prob_type_r = prob_type[0]
        prob_time_r = float(prob_time[random_time])
        dis_event = self.creat_events(prob_type_r, 'Generator_Trip')
        dis_event.time = prob_time_r # fault time

        # Random generator tripping
        if location == -1:
            dis_event.p_target = self.generators[self.generators_dic[self.prob_location_r]]

        # Designated generator tripping
        else:
            dis_event.p_target = self.generators[self.generators_dic[prob_location[location]]]
            self.prob_location_r = prob_location[location]
        dis_event.i_what = 0
        self.generator_out_t="True"

    def get_reward(self):
        """
        Reward function is the key to the effectiveness of DRL training,
        users can design their own reward function according to their own needs
        """
        reward=0
        for i in range(len(self.bus_f.values)):
            reward=-abs(self.bus_f.values[i]-50)
        reward = round(copy.deepcopy(reward),4)*100

        # Line Overload Penalty
        for row in self.line_P.iterrows():
            if abs(row[1].values.copy()) >= 380:
                reward-=10
        if self.penalty_line == 1:
            self.penalty_label = 1
            self.penalty_line = 0

        # Penalty for phase change failure
        Uaci=(2*self.x*self.i)/((2**0.5)*0.5*(self.sa-self.a))
        if self.U.values<Uaci:
            reward-=10
        return reward

    def DC_support(self,DC_action):
        # Parsing and executing EPS commands based on DRL signals
        evt_type = 'EvtParam'
        Delta_P_object = self.app.GetCalcRelevantObjects("Rect Controller_HVDC2.ElmDsl")[0]
        ls_event = self.creat_events(evt_type, 'Delta_P' + str(self.step_))
        ls_event.time = DC_action_time + (action_step_time * (self.history_step_time - 1) + sim_frist_step)  # Time of EPS
        ls_event.p_target = Delta_P_object
        ls_event.variable = "Delta_P"
        ls_event.value = str(DC_action*10+DC_low)  # Amount of EPS

    def step(self, action):
        """
        Execute the EPS command
        Control the power system to step one simulation step
        Send back a segment of observation data
        Some additional rewards
        :param action: DRL signals
        :return: Observational data, Reward, Whether to complete the current training episode
        """
        action_ = action.copy()
        print(self.total_step_)

        if action_ == 0:
            self.sim.SaveSnapshot()
            observation = self.pf_forward()  # No EPS order
        else:
            self.DC_support(action_-1) # Parsing and executing EPS commands based on DRL signals
            self.sim.SaveSnapshot()
            observation = self.pf_forward()
        if self.step_==1:
            action_last=action_.copy()

        reward=self.get_reward()

        # Limiting the EPS adjustment
        if self.step_>=2:
            if abs(action_last-action_)>20:
                reward-=10
            action_last = action_.copy()

        # Order penalty
        if action!=0:
            reward-=-0.1-1.8*self.step_

        # System crash penalty
        if self.break_report == 1:
            self.break_report = 0
            reward -= 100

        self.step_ += 1
        self.total_step_ += 1
        if self.total_step_ % delete_model_time <= total_time_step:
            self.delete_model_times=1

        if self.step_ > self.total_time_step:
            done = True
        else :
            done = False
        return observation, reward, done, {}

    def close(self):
        pass
