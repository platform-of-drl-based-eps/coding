Deep Reinforcement Learning-based Emergency DC Power Support Project Report.

In this project, we explore the use of deep reinforcement learning (DRL) methods for solving frequency control problems by using emergency DC power support (EPS) in power systems. We utilize the PowerFactory2022 (the Power System Simulation Software) (https://www.digsilent.de/en/powerfactory.html) as a power system simulator. We developed an stable-baselines3 (https://stable-baselines3.readthedocs.io/en/master/index.html) compatible grid dynamics simulation environment for developing, testing and benchmarking reinforcement learning algorithms for grid control.

Environment setup
To run the training, you need python 3.7 or above. Windows11-based OS is recommended. We suggest using Anaconda to create virtual environment.
Packages to be installed for the virtual environment:
1. stable-baselines3
2. gym[all]
3. git
4. swig
5. Box2D
6. pyglet==1.2.4
7. tensorboard

Training
1. The platform has been done with a high degree of integration. Most of the parameters for platform operation can be edited in the json file called "parameters_EPS.json", including EPS Execution Time, DC Power Support Lower and Upper Limit, Faults, interaction interval, Simulation step, How many interactions are performed on a single training episode, etc..
2. Download the given file to the unified directory.
3. After installing the virtual environment and PowerFactory, user can import the reference power system model "IEEE39.pfd" into PowerFactory and make sure that the PowerFactory language is English. Users need to open the imported pdf file in the PowerFactory2022, click "Study Cases" > "Power Flow" > "Result Export" > "file name", change the path to the downloaded "temp_results.csv" file and click "Apply". Users also need to do the same thing in "Study Cases" > "Power Flow" > "ASCII Result Export" > "file name".
4. Open the files "environment_EPS.py". There are two paths in the code that need to be modified: the PowerFactory2022 installation path and the path where the power system model are stored (It is critical to ensure that the path is accurate). Most of the parameters used in the "environment_EPS.py" file are set in the json file, except for the reward function, reward gradient, classification of state and state gradient. 
5. The “run.py” file is used to set the hyperparameters of the DRL and to start the platform running. It is easy to switch between training without model, training with model and application modes. Training without modeling mode refers to training with a brand new DRL neural network. Modeled training mode refers to further training based on the originally trained model. The application mode refers to calling the trained model to verify the control effect. Simply run the file to start the automated simulation and training of the entire platform.



